Vue.component('kev-grid', {
    template: `
<div>
<table class="kev-grid-table" :id="tableId">
<thead>
    <tr class="kev-grid-thead-row">
        <th 
        v-for="(c,cId) in columnDefs" 
        :key="cId" 
        v-on:click="sort(c.name,c.dataType)"
        >{{c.title}}</th>
    </tr>
</thead>
<tbody>
    <tr  class="kev-grid-tbody-row"
    v-for="(d,dId) in rowsToShow" 
    :key="dId" 
    :id="'row_' + dId">
        <td 
        v-for="(c,cId) in columnDefs" 
        :key="cId"
        :id="c.name"
        
        >{{ render(d[c.name], c) }}</td>
    </tr>
</tbody>
</table>
<div class="kev-grid-paging">

  <span class="prev" style="cursor:pointer;" v-on:click="currentPage-=1">  Prev</span>

  <span class="first" style="cursor:pointer;" v-on:click="currentPage=1"> 1 </span> 

  <span class="first" style="cursor:pointer;" v-on:click="currentPage=2"> 2 </span> 

  <span class="first" style="cursor:pointer;" v-on:click="currentPage=3"> 3 </span> 

  <span class="next" style="cursor:pointer;" v-on:click="currentPage+=1">  Next</span>

  <span class="last" style="cursor:pointer;" v-on:click="currentPage = lastPage">  Last</span>
</div>
</div>
`,
    props: ['data', 'columns', 'tableId', 'pageCount'],
    data: function () {
        return {
            columnDefs: this.columns,
            dataArray: this.data,
            sortAsc: true,
            sortProperty: '',
            pageSize: this.pageCount || 10,
            currentPage: 1
        }
    },
    computed:{
        lastPage: function(){
            return Math.ceil(this.dataArray.length / this.pageSize);
        },
        rowsToShow: function(){
            if(this.currentPage < 1) this.currentPage = 1;
            if(this.pageSize < 1) this.pageSize = 1;
            if(this.currentPage > this.lastPage) this.currentPage = this.lastPage;

            let from = ((this.currentPage - 1) * this.pageSize) ;
            let to = this.currentPage * this.pageSize;
            return this.dataArray.slice(from, to);
        }
    },
    methods: {
        getPropertyNames: function (obj) {
            return Object.getOwnPropertyNames(obj);
        },
        sort: function (field, dataType) {
            this.sortAsc = !this.sortAsc;
            this.sortProperty = field;

            console.info(`sorting by ${dataType} ${field} ${this.sortAsc ? 'asc' : 'desc'}`);

            var copy = this.dataArray;
            this.dataArray = [];

            var compareFnc = null;
            var safeKey = (dataType || 'string').toUpperCase();
            switch (safeKey) {
                case 'DATE':
                    compareFnc = this.compareDate;
                    break;
                case 'NUMBER':
                    compareFnc = this.compareNum;
                    break;
                default:
                    compareFnc = this.compareString;
                    break;
            }
            copy.sort(compareFnc);
            this.dataArray = copy;

        },
        compareString: function (a, b) {
            const nameA = a[this.sortProperty].toUpperCase();
            const nameB = b[this.sortProperty].toUpperCase();

            if (this.sortAsc) {
                if (nameA < nameB) return -1;
                if (nameA > nameB) return 1;
            }
            else {
                if (nameA < nameB) return 1;
                if (nameA > nameB) return -1;
            }
            return 0;
        },
        compareNum: function (a, b) {
            const numA = a[this.sortProperty];
            const numB = b[this.sortProperty];

            if (this.sortAsc) {

                return numA - numB;
            }
            else {
                return numB - numA;
            }

        },
        compareDate: function (a, b) {
            const dtA = new Date(a[this.sortProperty]);
            const dtB = new Date(b[this.sortProperty]);

            if (this.sortAsc) {
                if (dtA < dtB) return -1;
                if (dtA > dtB) return 1;
            }
            else {
                if (dtA < dtB) return 1;
                if (dtA > dtB) return -1;

            }
            return 0;
        },
        render: function (val, columnDefinition) {
            if (columnDefinition && columnDefinition.renderFn && typeof columnDefinition.renderFn === 'function') {
                return columnDefinition.renderFn(val);
            }
            else{ return val;}
        }
    }
});